import React, {Component} from 'react';
import {
    Modal,
    Button,
    message,
    Input,
    Form,
    Tabs
} from "antd";
const {TabPane} = Tabs;

export const EditShopModalForm = ({visible, data, onSubmit, onCancel,test}) => {
   
    //console.log(`DATA ID:${data?data.id:'-'}`)
    const [form] = Form.useForm();
    form.setFieldsValue(data)
    const formItemLayout = {
        labelCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 6
            }
        },
        wrapperCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 12
            }
        }
    };
    return (
        <Modal
            title="อัพเดทข้อมูลร้าน"
            centered
            forceRender={true}
            visible={visible}
            afterClose={()=>{
                //console.log('afterclose')
                //form.resetFields();
            }}
            okText="Submit"
            onCancel={()=>{
                onCancel('edit')
                form.resetFields();
            }}
            onOk={() => {
            form
                .validateFields()
                .then(values => {
                    onSubmit(values);
                })
                .catch(info => {
                    //console.log('Validate Failed:', info);
                });
        }}>
            <Form {...formItemLayout} form={form} >
                <Form.Item
                    label="รหัสร้าน"
                    name="id"
                    rules={[{
                        required: true
                    }
                ]}>
                    <Input value={data?data.id:''} disabled={true}/>
                </Form.Item>
                <Tabs defaultActiveKey="1">
                    <TabPane tab="ข้อมูล Stock หน้ากาก" key="1">
                        <Form.Item
                            label="จำนวนหน้ากาก"
                            name="quantity"
                            rules={[{
                                required: true,
                                message: 'กรุณาระบุข้อมูล!'
                            }
                        ]}>
                            <Input/>
                        </Form.Item>
                        <Form.Item
                            label="ราคาต่อชิ้น"
                            name="price"
                            rules={[{
                                required: true,
                                message: 'กรุณาระบุข้อมูล!'
                            }
                        ]}>
                            <Input/>
                        </Form.Item>
                    </TabPane>
                    <TabPane tab="ข้อมูลร้าน" key="2">
                        <Form.Item
                            label="ชื่อร้าน"
                            name="name"
                            rules={[{
                                required: true,
                                message: 'กรุณาระบุข้อมูลชื่อ!'
                            }
                        ]}>
                            <Input/>
                        </Form.Item>
                        <Form.Item
                            label="ที่อยู่"
                            name="address"
                            rules={[{
                                required: true,
                                message: 'กรุณาระบุข้อมูล!'
                            }
                        ]}>
                            <Input/>
                        </Form.Item>
                        <Form.Item
                            label="เบอร์โทร"
                            name="phone"
                            rules={[{
                                required: true,
                                message: 'กรุณาระบุข้อมูล!'
                            }
                        ]}>
                            <Input/>
                        </Form.Item>
                    </TabPane>
                    <TabPane tab="พิกัดร้าน" key="3">
                    <Form.Item
                            label="Latitude"
                            name="lat"
                            rules={[{
                                required: true,
                                message: 'กรุณาระบุข้อมูล!'
                            }
                        ]}>
                            <Input/>
                        </Form.Item>
                        <Form.Item
                            label="Longitude"
                            name="lng"
                            rules={[{
                                required: true,
                                message: 'กรุณาระบุข้อมูล!'
                            }
                        ]}>
                            <Input/>
                        </Form.Item>
                    </TabPane>
                </Tabs>
            </Form>
        </Modal>
    )
}