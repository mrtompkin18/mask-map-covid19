import React, {Component} from 'react';
import {Modal, Button, Row, Col, Card} from "antd";

const colorByLevel = (quantity) => {
    var color = '#262626'
    if (quantity > 0 && quantity <= 500) {
        color = '#851d41'
    } else if (quantity > 500 && quantity <= 2500) {
        color = '#df8543'
    } else if (quantity > 2500 && quantity <= 5000) {
        color = '#ecce6d'
    } else if (quantity > 5000 && quantity <= 10000) {
        color = '#4dd599'
    } else if (quantity > 10000) {
        color = '#3fc5f0'
    } else {
        color = '#262626'
    }
    return color
}
export const InfoPopup = ({visible, data, onGoogleMap, onCall, onCancel}) => {

   

    return (
        <Modal
            title={data
            ? `ร้าน: ${data.name}`
            : ''}
            centered
            forceRender={true}
            visible={visible}
            onCancel={() => {
            onCancel('info')
        }}
            afterClose={() => {
            //console.log('afterclose')
        }}
            footer={[(
                <Button
                    key="back"
                    onClick={() => {
                    onCancel('info')
                }}>
                    Back
                </Button>
            ), (
                <Button
                    key="google"
                    onClick={() => {
                    onGoogleMap()
                }}>
                    ดูเส้นทาง
                </Button>
            ), (
                <Button
                    key="call"
                    type="primary"
                    onClick={() => {
                    onCall()
                }}>
                    โทร
                </Button>
            )]}>
            <Row gutter={16}>
                <Col span={14}>
                    <Card size="small" title="จำนวนหน้ากาก/ชิ้น" bordered={false}>
                        <p
                            style={{
                            fontSize: 24,
                            fontWeight: 800,
                            color:data?colorByLevel(data.quantity):'#282828'
                        }}>{data
                                ? data.quantity
                                : ''}</p>
                    </Card>
                </Col>
                <Col span={10}>
                    <Card size="small" title="ราคาต่อชิ้น" bordered={false}>
                        <p
                            style={{
                            fontSize: 24,
                            fontWeight: 800,
                            textAlign: 'center'
                        }}>{data
                                ? `${data.price} บ.`
                                : ''}</p>
                    </Card>
                </Col>
                <Col span={24}>
                    <p style={{
                        textAlign: 'right'
                    }}>{data
                            ? `โทร. ${data.phone}`
                            : ''}</p>
                </Col>
            </Row>
        </Modal>
    )
}