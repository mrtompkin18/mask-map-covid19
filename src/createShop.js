import React, {Component} from 'react';
import {
    Modal,
    Input,
    Form
} from "antd";

export const CreateShopModalForm = ({visible, onSubmit, onCancel}) => {
    const [form] = Form.useForm();
    const formItemLayout = {
        labelCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 6
            }
        },
        wrapperCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 12
            }
        }
    };
    return (
        <Modal
            title="สร้างข้อมูลร้านใหม่"
            visible={visible}
            centered
            okText="Submit"
            afterClose={()=>{
                //console.log('afterclose')
                form.resetFields();
            }}
            onCancel={()=>{
                form.resetFields();
                onCancel('create')
            }}
            onOk={() => {
            form
                .validateFields()
                .then(values => {
                    onSubmit(values);
                })
                .catch(info => {
                    //console.log('Validate Failed:', info);
                });
        }}>
            <Form {...formItemLayout} form={form}>
                <Form.Item
                    label="ชื่อร้าน"
                    name="name"
                    rules={[{
                        required: true,
                        message: 'กรุณาระบุข้อมูลชื่อ!'
                    }
                ]}>
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="ที่อยู่"
                    name="address"
                    rules={[{
                        required: true,
                        message: 'กรุณาระบุข้อมูล!'
                    }
                ]}>
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="เบอร์โทร"
                    name="phone"
                    rules={[{
                        required: true,
                        message: 'กรุณาระบุข้อมูล!'
                    }
                ]}>
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="จำนวนหน้ากาก"
                    name="quantity"
                    extra="ใส่จำนวนชิ้นทั้งหมด เป็นตัวเลข"
                    rules={[{
                        required: true,
                        message: 'กรุณาระบุข้อมูล!'
                    }
                ]}>
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="ราคาต่อชิ้น"
                    name="price"
                    extra="ใส่ราคาต่อชิ้น ไม่ต้องใส่หน่วย"
                    rules={[{
                        required: true,
                        message: 'กรุณาระบุข้อมูล!'
                    }
                ]}>
                    <Input/>
                </Form.Item>
            </Form>
        </Modal>
    )
}